FROM jekyll/builder AS build
RUN apk add --no-cache go
RUN gem install bundler:1.16.4
ENV JEKYLL_LOG_LEVEL debug

COPY server /var/src/web
WORKDIR /var/src/web
RUN go build -a -o /usr/bin/web .

COPY run.sh /

RUN mkdir /var/www && chown 1000:1000 /var/www

WORKDIR /var/www
ENV ENABLE_COMPRESSION cached
CMD ["/run.sh"]