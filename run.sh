#!/bin/sh
set -e
if ! [ -d "/srv/jekyll/.git" ]; then
	echo "First-time setup..."
	git clone "$GIT_REPOSITORY" /srv/jekyll
	if [ -z "$GIT_BRANCH" ]; then
		GIT_BRANCH="main"
	fi
	(
		cd /srv/jekyll;
		git checkout "origin/$GIT_BRANCH";
		echo "Installing dependencies...";
		bundle install;
	)
fi
exec web
