module codeberg.org/momar/jekyll-deployment/server

go 1.14

require (
	codeberg.org/momar/web v0.0.0-20210227104111-3549de88ab62
	github.com/valyala/fasthttp v1.21.0
)
