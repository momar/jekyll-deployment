package main

import (
	"codeberg.org/momar/web"
	"context"
	"fmt"
	"github.com/valyala/fasthttp"
	"io"
	"os"
	"os/exec"
	"strings"
	"time"
)

var dir = "/srv/jekyll"
func run(ctx context.Context, stdout io.Writer, program string, args... string) error {
	if rctx, _ := ctx.(*fasthttp.RequestCtx); ctx == nil || rctx == nil {
		ctx = context.Background()
	}
	cmd := exec.CommandContext(ctx, program, args...)
	cmd.Dir = dir
	cmd.Stdout = stdout
	cmd.Stderr = stdout
	stdout.Write([]byte("Executing command: " + program + " " + strings.Join(args, " ") + "\n"))
	err := cmd.Run()
	if err != nil {
		if ctx, ok := ctx.(*fasthttp.RequestCtx); ok && ctx != nil {
			ctx.SetStatusCode(500)
		}
		stdout.Write([]byte("Error: " + err.Error() + "\n"))
	}
	return err
}

var working = false
var queued = false
func build(ctx *fasthttp.RequestCtx) {
	var out io.Writer = os.Stdout
	if ctx != nil {
		out = io.MultiWriter(
			out,
			ctx.Response.BodyWriter(),
		)
	}
	
	if working {
		queued = true
		if ctx != nil {
			out.Write([]byte("Queued a build as one is running right now.\n"))
		}
		return
	}
	working = true
	out.Write([]byte("Starting build at " + time.Now().UTC().Format("2006-01-02 15:04:05 UTC") + ".\n"))

	branch := os.Getenv("GIT_BRANCH")
	if branch == "" {
		branch = "main"
	}

	if err := run(ctx, out, "git", "fetch"); err != nil {
		return
	}

	bundleChanges := run(ctx, out, "git", "diff", "--exit-code", "origin/" + branch, "--", "Gemfile", "Gemfile.lock")
	
	if err := run(ctx, out, "git", "reset", "--hard", "origin/" + branch); err != nil {
		return
	}

	if bundleChanges != nil {
		if err := run(ctx, out, "bundle", "install"); err != nil {
			return
		}
	}

	wd, _ := os.Getwd()
	if err := run(ctx, out, "bundle", "exec", "jekyll", "build", "-d", wd); err != nil {
		return
	}
	out.Write([]byte("Done at " + time.Now().UTC().Format("2006-01-02 15:04:05 UTC") + ".\n"))

	working = false
	if queued {
		queued = false
		go build(nil)
	}
}

func main() {
	build(nil)
	if err := web.Run(func(ctx *fasthttp.RequestCtx) bool {
		if string(ctx.Path()) == "/.well-known/build-webhook/" + os.Getenv("WEBHOOK_TOKEN") {
			build(ctx)
			return false
		}
		return true
	}); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
