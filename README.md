# Jekyll Deployment Image

All-in-one web server that builds & serves a [Jekyll](https://jekyllrb.com/) site from a Git
repository, and auto-updates through webhooks. Based on [momar/web](https://codeberg.org/momar/web).

## Usage
```bash
docker run \
  --name jekyll-deployment \
  -d -p 80:80 \
  -e WEBHOOK_TOKEN=+++ \
  -e GIT_REPOSITORY=https://codeberg.org/+++/+++.git \
  -e GIT_BRANCH=main \
  momar/jekyll-deployment
```

You can generate a webhook token for example with `tr -dc A-Za-z0-9 < /dev/urandom | head -c 64`.  
The webhook will listen on `/.well-known/build-webhook/$WEBHOOK_TOKEN`, and can e. g. be added as a
webhook in the repository settings of Gitea to automate the build (additional settings basically
don't matter, but you should probably set a branch filter).
